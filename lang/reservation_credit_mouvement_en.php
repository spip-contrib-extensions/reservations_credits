<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/reservation_credit_mouvement?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_reservation_credit_mouvement' => 'Add this credit movement',

	// C
	'champ_credit_de' => 'Credit from:',
	'champ_date_creation_label' => 'Creation date:',
	'champ_descriptif_label' => 'Description:',
	'champ_devise_label' => 'Currency :',
	'champ_type_label' => 'Type',

	// I
	'icone_creer_reservation_credit_mouvement' => 'Create a credit movement',
	'icone_modifier_reservation_credit_mouvement' => 'Modify this credit movement',
	'info_1_reservation_credit_mouvement' => 'A credit movement',
	'info_aucun_reservation_credit_mouvement' => 'No credit movement',
	'info_nb_reservation_credit_mouvements' => '@nb@ credit movements',
	'info_reservation_credit_mouvements_auteur' => 'The credit movements of this author',

	// M
	'mouvement_evenement_annule' => 'Cancellation event "@titre@"',
	'mouvement_evenement_publie' => 'Reprogramming event "@titre@"',

	// R
	'retirer_lien_reservation_credit_mouvement' => 'Remove this credit movement',
	'retirer_tous_liens_reservation_credit_mouvements' => 'Remove all credit movements',

	// T
	'texte_ajouter_reservation_credit_mouvement' => 'Add a credit movement',
	'texte_changer_statut_reservation_credit_mouvement' => 'This credit movement is:',
	'texte_creer_associer_reservation_credit_mouvement' => 'Add and link a credit movement',
	'texte_definir_comme_traduction_reservation_credit_mouvement' => 'This credit movement is a translation of credit movement number:',
	'titre_langue_reservation_credit_mouvement' => 'Language of this credit movement',
	'titre_logo_reservation_credit_mouvement' => 'Logo of this credit movement',
	'titre_reservation_credit_mouvement' => 'Credit movement',
	'titre_reservation_credit_mouvements' => 'Credit movements',
	'titre_reservation_credit_mouvements_rubrique' => 'Credit movements  of this section',
	'type_credit' => 'Credit (increases the credit amount)',
	'type_debit' => 'Debit (reduces the credit amount)'
);
